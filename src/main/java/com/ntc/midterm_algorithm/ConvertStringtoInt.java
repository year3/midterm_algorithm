package com.ntc.midterm_algorithm;

import java.util.Scanner;
import java.lang.Integer;

/**
 *
 * @author L4ZY
 */
public class ConvertStringtoInt {

    public static void main(String[] args) {
        long start = System.nanoTime(); //time Start
        Scanner kb = new Scanner(System.in); //Scanner input

        String string = kb.nextLine();//inputString
        System.out.println(ConvertStringtoInt(string)); //Output sum
        long end = System.nanoTime();
        System.out.println("Running Time is " + (end - start) * 1E-9 + " secs.");//Output Time

    }

    public static int ConvertStringtoInt(String string) {
        int length = string.length() - 1;
        int sum = 0;

        for (int i = 0; i <= length; i++) {
            sum += (string.charAt(i) - 48) * Math.pow(10, length - i);
            //select chaAt and subtract with first Character of number is 0
            //and next power with  Ordinal number
            /*  char               ASCII   
                '0'                       48         
                '1'                       49          
                '2'                       50           
                '3'                       51           
                '4'                       52           
                '5'                       53           
                '6'                       54           
                '7'                       55           
                '8'                       56           
                '9'                       57         */
        }
        return sum;
    }

}
